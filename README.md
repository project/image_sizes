# Image Sizes

Responsive image styles depend on the parent element width.

Features:
   - Add a new image effect "insert_style"
   - Add new image field formatter
   - Add custom presets for choose on website

For a full description of the module, visit the
[project page](https://www.drupal.org/project/image_sizes).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/image_sizes).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following outside of Drupal core:

- oomphinc/composer-installers-extender


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

To install all assets, please add "https://asset-packagist.org" to your composer file.

```
  "repositories": {
    "drupal": {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
      },
    "assets": {
      "type": "composer",
      "url": "https://asset-packagist.org"
    }
  },
  ```
Or

`composer config repositories.assets-packagist composer https://asset-packagist.org`

Add oomphinc/composer-installers-extender

`composer require oomphinc/composer-installers-extender:^1.1`

And set installer types and paths:


"extras": {
    "installer-types": ["bower-asset", "npm-asset"],
     "installer-paths": {
         "docroot/libraries/{$name}": ["type:bower-asset", "type:npm-asset"]
     }
}
```


## Maintainers

- Erik Seifert - [Erik Seifert](https://www.drupal.org/u/erik-seifert)

Supporting organizations:

- [b-connect GmbH](https://www.drupal.org/b-connect-gmbh)
