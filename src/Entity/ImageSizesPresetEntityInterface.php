<?php

namespace Drupal\image_sizes\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Image sizes preset entity entities.
 */
interface ImageSizesPresetEntityInterface extends ConfigEntityInterface {

  /**
   * Setter fallback style.
   *
   * @param string $fallback
   *   Fallback style name.
   */
  public function setFallbackStyle($fallback);

  /**
   * Get fallback style.
   *
   * @return string
   *   Fallback style name.
   */
  public function getFallbackStyle();

  /**
   * Setter preload style.
   *
   * @param string $preload
   *   Preload style name.
   */
  public function setPreloadStyle($preload);

  /**
   * Get preload style.
   *
   * @return string
   *   Preload style name.
   */
  public function getPreloadStyle();

  /**
   * Set styles.
   *
   * @param string[] $styles
   *   Fallback style name.
   */
  public function setStyles(array $styles);

  /**
   * Get styles.
   *
   * @return string[]
   *   All styles.
   */
  public function getStyles();

  /**
   * Add a new style.
   *
   * @param string $style
   *   Style name to add.
   */
  public function addStyle($style);

  /**
   * Remove a style.
   *
   * @param string $style
   *   Style name to remove.
   */
  public function removeStyle($style);

}
